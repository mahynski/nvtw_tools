"""
Tools to compute the macrostate distribution from multicomponent NVT+W simulations.
"""
import os
import sys
import copy
import math
import scipy.optimize
import numpy as np

import cython
cimport numpy as np
cimport cython
cimport libc.math

class CollectionMatrix(object):
    """
    Object to treat sparse collection matrix efficiently.
    """
    def __init__(self, nspecies):
        """
        Parameters
        ----------
        nspecies : int
            Number of species
        """
        self.__nspecies = nspecies
        self.__lnP = {}
        self.__array = {}

    def __call__(self, start, finish):
        """
        When called, return the value of the collection matrix.  If the transition
        (start --> finish) does not exist, a value of 0 is returned.

        Parameters
        ----------
        start : tuple(int)
            Initial (N1, N2, ..., Nk) state
        finish : tuple(int)
            Final (N1, N2, ..., Nk) state

        Returns
        -------
        float
            Value of the collection matrix, C(start --> finish)
        """
        assert(len(start) == self.__nspecies), 'coordinate mis-specified'
        assert(len(finish) == self.__nspecies), 'coordinate mis-specified'
        if (start not in self.__array):
            return 0.0
        if (finish not in self.__array[start]):
            return 0.0
        return self.__array[start][finish]

    @cython.boundscheck(False)
    @cython.cdivision(True)
    @cython.wraparound(False)
    def _fast_call(self, start, finish):
        """
        Same as __call__ but doesn't do any consistency checks.
        """
        if (start not in self.__array):
            return 0.0
        if (finish not in self.__array[start]):
            return 0.0
        return self.__array[start][finish]

    @property
    def nspecies(self):
        """
        Returns
        -------
        int
            Number of species
        """
        return self.__nspecies

    def transitions(self, start):
        """
        All transitions (final states) from a given initial state.

        Parameters
        ----------
        start : tuple(int)
            Initial (N1, N2, ..., Nk) state

        Returns
        -------
        dict
            Dictionary of final state, value {tuple(int) : float}
        """
        assert(len(start) == self.__nspecies), 'coordinate mis-specified'
        if (start not in self.__array):
            return {}
        else:
            return self.__array[start]

    def set(self, start, finish, value, overwrite=True):
        """
        Assign collection matrix value.

        Parameters
        ----------
        start : tuple(int)
            Initial (N1, N2, ..., Nk) state
        finish : tuple(int)
            Final (N1, N2, ..., Nk) state
        value : float
            C(start --> final) value
        overwrite : bool
            If False, does not allow a value to overwrite a previous entry for
            these (start, finish) conditions.  Set to False to ensure only single
            data entry.
        """
        assert(value >= 0.0), 'invalid collection matrix value'
        assert(len(start) == self.__nspecies), 'coordinate mis-specified'
        assert(len(finish) == self.__nspecies), 'coordinate mis-specified'

        if (start not in self.__array):
            self.__array[start] = {}
        if (finish not in self.__array[start]):
            self.__array[start][finish] = 0.0
        else:
            if (not overwrite):
                raise Exception("start = {}, finish = {} already entered".format(start, finish))

        self.__array[start][finish] = value

    def update(self, start, finish, p_acc):
        """
        Update collection matrix with (unbiased) acceptance probability.

        Parameters
        ----------
        start : tuple(int)
            Initial (N1, N2, ..., Nk) state
        finish : tuple(int)
            Final (N1, N2, ..., Nk) state
        p_acc : float
            Unbiased acceptance criteria
        """
        assert(0.0 <= p_acc <= 1.0), 'invalid acceptance probability'
        assert(len(start) == self.__nspecies), 'coordinate mis-specified'
        assert(len(finish) == self.__nspecies), 'coordinate mis-specified'

        if (start not in self.__array):
            self.__array[start] = {}
        if (finish not in self.__array[start]):
            self.__array[start][finish] = 0.0
            self.__array[start][start] = 0.0

        self.__array[start][finish] += p_acc
        self.__array[start][start] += (1.0-p_acc)

    @cython.boundscheck(False)
    @cython.cdivision(True)
    @cython.wraparound(False)
    def _fast_lnP(self, start, finish, cache=True):
        """
        Same as lnP() but has some accelerations built in.
        """
        cdef np.float64_t denom = 0.0
        if ((start,finish) not in self.__lnP or not cache):
            trans = self.transitions(start)
            denom = np.sum([trans[k] for k in trans], dtype=np.float64)
            if (denom < 1.0e-12):
                raise Exception('transitions not measured from {}'.format(start))
            else:
                if (trans[finish] > 0.0):
                    self.__lnP[(start,finish)] = libc.math.log(trans[finish]/denom)
                else:
                    self.__lnP[(start,finish)] = np.finfo(np.float64).min
        return self.__lnP[(start,finish)]

    def lnP(self, start, finish, cache=True):
        """
        Get (natural logarithm of) the probability of a transition.
        This uses caching by default, so this is safe only if the collection
        matrix is no longer going to be updated.  Turn off caching to compute
        every time.

        Parameters
        ----------
        start : tuple(int)
            Initial (N1, N2, ..., Nk) state
        finish : tuple(int)
            Final (N1, N2, ..., Nk) state
        cache : bool
            Use caching to store the value of lnP once calculated

        Returns
        -------
        float
            Natural log of transition probability, ln(P)
        """
        # Check if cached already
        if ((start,finish) not in self.__lnP or not cache):
            trans = self.transitions(start)
            denom = np.sum([trans[k] for k in trans], dtype=np.float64)
            if (denom < 1.0e-12):
                raise Exception('transitions not measured from {}'.format(start))
            else:
                if (trans[finish] > 0.0):
                    self.__lnP[(start,finish)] = np.log(trans[finish]/denom)
                else:
                    self.__lnP[(start,finish)] = np.finfo(np.float64).min
        return self.__lnP[(start,finish)]

cdef inline np.float64_t _double_max(np.float64_t a, np.float64_t b): return a if a > b else b

@cython.boundscheck(False)
@cython.cdivision(True)
def _spec_exp(a, b):
    r"""
    Compute the natural logarithm of the sum of a pair of exponentials.  i.e. :math:`{\rm ln}\left( {\rm exp}(a) + {\rm exp}(b) \right)`.

    Parameters
    ----------
    a : double
        Argument of first exponential.
    b : double
        Argument of second exponential.

    Returns
    -------
    double
        :math:`{\rm ln} \left( {\rm exp}(a) + {\rm exp}(b) \right)`.
    """

    return _double_max(a, b) + libc.math.log(1.0 + np.exp(-libc.math.fabs(a-b)))

def create_binary_collection_matrix(data):
    """
    Create collection matrix from raw output of RASPA's acceptance criteria
    between "neighboring" canonical states.

    Parameters
    ----------
    data : dict(tuple,str) or str
        Dictionary with keys of N-tuples (NVT conditions) and values of filenames,
        or filename (string) with all data as rows in that file.

    Returns
    -------
    dict(ndarray, list, dict)
        Collection matrix, list of (min,max) bounds for each component, dictionary of the
        max value of the last species index for the hyperplane defined by the
        remainder of the species (e.g., max(N3) at a given (N1,N2))

    Example
    -------
    >>> d = {(0,1):"/path/to/output/acceptance_01.txt",
    ...      (1,1):"/path/to/output/acceptance_11.txt",
    ...
    ...      (9,1):"/path/to/output/acceptance_90.txt",
    ...      (0,2):"/path/to/output/acceptance_02.txt",
    ...
            }
    >>> res = create_binary_collection_matrix(d)

    or

    >>> res = create_binary_collection_matrix("output.txt")
    """

    if (isinstance(data, dict)):
        # Assume data provided in a single file for each NVT state

        # Compute the number of components present and their (min,max) bounds
        N = -1
        limiting_Nf = {}
        for nvt_state in data:
            n = len(nvt_state)
            if (N < 0):
                N = n
                min_bound = np.zeros(N, dtype=np.int32)
                min_bound.fill(np.iinfo(np.int32).max)
                max_bound = np.zeros(N, dtype=np.int32)
            else:
                if (n != N):
                    raise Exception('all keys must be provided as a tuple of N values for all components')

            for i in range(N):
                min_bound[i] = int(np.min([min_bound[i], nvt_state[i]]))
                max_bound[i] = int(np.max([max_bound[i], nvt_state[i]]))

            if (nvt_state[:-1] not in limiting_Nf):
                limiting_Nf[nvt_state[:-1]] = int(nvt_state[-1])
            else:
                limiting_Nf[nvt_state[:-1]] = int(np.max([nvt_state[-1], limiting_Nf[nvt_state[:-1]]]))
        assert (N == 2), 'only accepts binary systems'

        # Go to each NVT location and read the average transition rates from RASPA
        C = CollectionMatrix(N)

        for nvt_state in data:
            # Read data from disk
            try:
                raw = np.loadtxt(data[nvt_state], dtype=np.float64)
            except Exception as e:
                raise Exception("unable to load {} : {}".format(data[nvt_state], e))

            # Update C matrix with this information
            n1, n2 = int(raw[1]), int(raw[2])
            if ((n1,n2) != nvt_state):
                raise Exception('mismatch in dictionary (n1,n2) = {} specified in {}'.format((n1,n2), nvt_state))
            a1, b1, c1 = raw[5:8]

            C.set((n1,n2), (n1+1,n2), a1, overwrite=False)
            C.set((n1,n2), (n1-1,n2), c1, overwrite=False)
            a2, b2, c2 = raw[10:13]
            C.set((n1,n2), (n1,n2+1), a2, overwrite=False)
            C.set((n1,n2), (n1,n2-1), c2, overwrite=False)
            C.set((n1,n2), (n1,n2), b1+b2, overwrite=False)
    else:
        # Assume data provided in one file as different rows

        # Read data from disk
        try:
            raw = np.loadtxt(data, unpack=False, dtype=np.float64)
        except Exception as e:
            raise Exception("unable to load {} : {}".format(data, e))

        nvals = [x[0] for x in raw]
        if (not np.all(nvals == nvals[0])):
            raise Exception('inconsistent number of species in {}'.format(data))
        N = int(nvals[0])
        assert (N == 2), 'only accepts binary systems'

        C = CollectionMatrix(N)

        min_bound = np.zeros(N, dtype=np.int32)
        min_bound.fill(np.iinfo(np.int32).max)
        max_bound = np.zeros(N, dtype=np.int32)
        limiting_Nf = {}

        for nvt_state in raw:
            n1, n2 = int(nvt_state[1]), int(nvt_state[2])
            a1, b1, c1 = nvt_state[5:8]
            C.set((n1,n2), (n1+1,n2), a1, overwrite=False)
            C.set((n1,n2), (n1-1,n2), c1, overwrite=False)
            a2, b2, c2 = nvt_state[10:13]
            C.set((n1,n2), (n1,n2+1), a2, overwrite=False)
            C.set((n1,n2), (n1,n2-1), c2, overwrite=False)
            C.set((n1,n2), (n1,n2), b1+b2, overwrite=False)

            nb = [n1,n2]
            for i in range(len(nb)):
                min_bound[i] = int(np.min([min_bound[i], nb[i]]))
                max_bound[i] = int(np.max([max_bound[i], nb[i]]))

            if ((n1,) not in limiting_Nf):
                limiting_Nf[(n1,)] = n2
            else:
                limiting_Nf[(n1,)] = int(np.max([n2, limiting_Nf[(n1,)]]))

    return {"collection_matrix":C, "bounds":zip(min_bound, max_bound), "limiting_Nf":limiting_Nf}

def binary_initial_guess(collection_matrix,
    bounds=None,
    limiting_Nf=None):
    """
    Use simple pathways to construct an initial guess at lnPI(N1,N2)
    from the collection matrix.  This assumes a continuous path and
    requires all N1,N2 states be sampled (to work the best).

    Parameters
    ----------
    collection_matrix : CollectionMatrix
        Collection matrix of data (see create_binary_collection_matrix()).
    bounds : list
        List of (min,max) bounds for each component (see create_binary_collection_matrix()).
    limiting_Nf : dict
        Dictionary of the max value of the last species index for the hyperplane defined by the
        remainder of the species (e.g., max(N3) at a given (N1,N2)); see create_binary_collection_matrix().

    Returns
    -------
    dict
        Dictionary of lnPI[n1][n2]
    """

    lnPI_g = np.zeros((int(bounds[0][1] - bounds[0][0])+1, int(bounds[1][1] - bounds[1][0])+1), dtype=np.float64)
    for n1 in range(int(bounds[0][0])+1, int(bounds[0][1])+1):
        # Walk from n1 = 1 to n1 = n1_max (n1 = 0 already set to zero by default above)
        n1_idx = n1 - int(bounds[0][0])
        lnPI_g[n1_idx,0] = lnPI_g[n1_idx-1,0] + \
            collection_matrix.lnP((n1-1,0),(n1,0)) - \
            collection_matrix.lnP((n1,0),(n1-1,0))

    for n1 in range(int(bounds[0][0]), int(bounds[0][1])+1):
        # For each n1, walk to the end of n2
        n1_idx = n1 - int(bounds[0][0])
        for n2 in range(int(bounds[1][0])+1, int(limiting_Nf[(n1,)])+1):
            # n2 = n2_min already filled by first "walk"
            n2_idx = n2 - int(bounds[1][0])
            lnPI_g[n1_idx,n2_idx] = lnPI_g[n1_idx,n2_idx-1] + \
                collection_matrix.lnP((n1,n2-1),(n1,n2)) - \
                collection_matrix.lnP((n1,n2),(n1,n2-1))

    # Convert to dictionary for future use
    lnPI_d = {}
    for n1 in range(int(bounds[0][0]), int(bounds[0][1])+1):
        if (n1 not in lnPI_d):
            lnPI_d[n1] = {}
        for n2 in range(int(bounds[1][0]), int(limiting_Nf[(n1,)])+1):
            lnPI_d[n1][n2] = lnPI_g[n1 - int(bounds[0][0]), n2 - int(bounds[1][0])]

    return lnPI_d

def compute_binary_macrostate_distribution(collection_matrix=None,
    bounds=None,
    limiting_Nf=None,
    options={'disp':True, 'maxiter':3000, 'return_all':True},
    initial_guess=None):
    """
    Compute the binary macrostate distribution, lnPi(N1, N2).
    Uses conjugate gradient optimization.

    Parameters
    ----------
    collection_matrix : CollectionMatrix
        Collection matrix to construct lnPi(N1, N2) based on
    bounds : list
        Array of (min, max) for each species
    limiting_Nf : dict
        Dictionary of the max value of the last species index for the hyperplane
        defined by the remainder of the species (e.g., max(N3) at a given (N1, N2))
    options : dict
        Dictionary of options for CG optimization
    initial_guess : dict
        Initial guess for lnPI(N1,N2), if computed.  See binary_initial_guess().

    Returns
    -------
    OptimizeResult, ndarray
        Result of optimization, normalized lnPi(N1, N2) corresponding to best optimization result, REGARDLESS
        of whether or not the optimization was successful.

    Notes
    -----
    Including {'return_all':True} in the options can be especially important as this
    allows you to watch the convergence of the optimization if it does not finish in a reasonable
    amount of time (or converge to it is desired tolerance).
    """
    collection_matrix = copy.deepcopy(collection_matrix) # Make a local copy so original remains unmodified
    nspecies = collection_matrix.nspecies
    assert (nspecies == 2), 'incorrect number of species'

    cdef np.int32_t bounds_00 = int(bounds[0][0]), bounds_01 = int(bounds[0][1]), bounds_10 = int(bounds[1][0])

    # Keep lnPI(0,0) fixed at 0, optimize other lnPI(N1,N2) values
    cdef np.float64_t lnPI_00
    if (initial_guess is None):
        lnPI_00 = 0.0
    else:
        lnPI_00 = initial_guess[bounds_00][bounds_10]

    # lnPI(N1,N2) will be flattened and we have to map an index back to (N1,N2) later
    flat_map = {}
    idx = -1
    lnPI_flattened_list = []
    for n1 in range(bounds_00, bounds_01+1):
        for n2 in range(bounds_10, int(limiting_Nf[(n1,)])+1):
            if (idx >= 0):
                if (initial_guess is None):
                    lnPI_flattened_list.append(0.0) # Default initial guess
                else:
                    lnPI_flattened_list.append(initial_guess[n1][n2])
            flat_map[(n1, n2)] = idx
            idx += 1
    cdef np.ndarray[np.float64_t, ndim=1] lnPI_flattened = np.array(lnPI_flattened_list, dtype=np.float64)

    @cython.boundscheck(False)
    @cython.cdivision(True)
    @cython.wraparound(False)
    def chi_squared(np.ndarray[np.float64_t, ndim=1] lnPI_flattened):
        cdef np.int32_t flat_start_idx, flat_final_idx, n1, n2
        cdef np.ndarray[np.float64_t, ndim=1] grad_chi_sq = np.zeros(len(lnPI_flattened), dtype=np.float64)
        cdef np.float64_t chi_sq = 0.0, lnPI_final, sqrt_factor, ln_factor, lnPI_start, a, b, c, d
        cdef np.int32_t bounds_00 = int(bounds[0][0]), bounds_01 = int(bounds[0][1]), bounds_10 = int(bounds[1][0])

        for n1 in range(bounds_00, bounds_01+1):
            for n2 in range(bounds_10, int(limiting_Nf[(n1,)])+1):
                start = (n1, n2)
                flat_start_idx = flat_map[start]
                if (flat_start_idx < 0):
                    lnPI_start = lnPI_00
                else:
                    lnPI_start = lnPI_flattened[flat_start_idx]

                for final in [(n1+1,n2),(n1-1,n2),(n1,n2+1),(n1,n2-1)]:
                    # If bounds go < 0 or above limit, collection matrix returns 0
                    try:
                        flat_final_idx = flat_map[final]
                    except KeyError:
                        # If, e.g., (-1,0) is encountered flat_map will not contain
                        # this final state.  However, this does not contribute to
                        # the calculation, so just skip this iteration
                        continue

                    if (flat_final_idx < 0):
                        lnPI_final = lnPI_00
                    else:
                        lnPI_final = lnPI_flattened[flat_final_idx]

                    a = collection_matrix._fast_call(start, final)
                    b = collection_matrix._fast_call(final, start)
                    c = collection_matrix._fast_lnP(start,final)
                    d = collection_matrix._fast_lnP(final,start)

                    sqrt_factor = libc.math.sqrt(a*b)
                    ln_factor = lnPI_start + c - lnPI_final - d

                    # X^2 is summed over all states, including (0,0)
                    chi_sq += sqrt_factor*(ln_factor**2)
                    if (flat_start_idx >= 0):
                        # Only record gradient for lnPI values that are variable
                        grad_chi_sq[flat_start_idx] += 4.0*sqrt_factor*ln_factor

        return chi_sq, grad_chi_sq

    # Minimize X^2
    res = scipy.optimize.minimize(fun=chi_squared, x0=lnPI_flattened, \
        method='CG',
        jac=True,
        options=options)

    # If res successful, add lnPI_00 in and normalize final distribution
    final_lnPI = np.zeros((int(bounds[0][1]-bounds[0][0])+1, int(bounds[1][1]-bounds[1][0])+1), dtype=np.float64)

    lnNormPI = -sys.float_info.max
    for n1 in range(int(bounds[0][0]), int(bounds[0][1])+1):
        n1_idx = n1 - int(bounds[0][0])
        for n2 in range(int(bounds[1][0]), int(limiting_Nf[(n1,)])+1):
            n2_idx = n2 - int(bounds[1][0])
            # if (n1_idx == 0 and n2_idx == 0):
            #     final_lnPI[n1_idx, n2_idx] = lnPI_00
            # else:
            #     final_lnPI[n1_idx, n2_idx] = res.x[flat_map[(n1,n2)]]

            flat_idx = flat_map[(n1,n2)]
            if (flat_idx >= 0):
                final_lnPI[n1_idx, n2_idx] = res.x[flat_idx]
            else:
                final_lnPI[n1_idx, n2_idx] = lnPI_00
            lnNormPI = _spec_exp (lnNormPI, final_lnPI[n1_idx, n2_idx])

    mask = final_lnPI == 0
    if (lnPI_00 == 0):
        mask[0,0] = False # Manually exclude this point from reassignment
    final_lnPI -= lnNormPI
    final_lnPI[mask] = -np.inf

    return res, final_lnPI

def binary_sanity_check(Ntot, collection_matrix):
    """
    Sanity checks for binary system.

    1. Check that all states up to, and including, N1+N2 <= Ntot have been assigned to collection_matrix.

    Parameters
    ----------
    collection_matrix : CollectionMatrix
        Collection matrix to check

    Returns
    -------
    list
        List of tuples of (n1,n2) transition data that is missing in collection_matrix

    Example
    -------
    >>> success = not pp.binary_sanity_check(59, res['collection_matrix'])
    >>> assert(success), 'bad data!'
    """

    missing = []
    for i in range(Ntot+1):
        for j in range(Ntot+1):
            if (i+j < Ntot+1):
                if (not bool(collection_matrix.transitions((i,j)))):
                    missing.append((i,j))

    return missing

if __name__ == "__main__":
    print os.path.basename(__file__)

    """
    Example
    -------
    >>> ans = create_binary_collection_matrix("output.txt")
    >>> assert(binary_sanity_check(Ntot, ans['collection_matrix']) == []), 'missing points!'
    >>> res, final_lnPI = compute_binary_macrostate_distribution(
            options={'disp':True, 'maxiter':3000, 'return_all':True},
            initial_guess=binary_initial_guess(**ans),
            **ans)
    """
