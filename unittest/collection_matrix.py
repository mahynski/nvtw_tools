import unittest
import sys
import os
import numpy as np
sys.path.append('../')
import patch

class TestCollectionMatrix(unittest.TestCase):
	"""
	Test collection matrix
	"""

	def setUp(self):
		pass

	def test_nspecies(self):
		C = patch.CollectionMatrix(2)
		self.assertEqual(C.nspecies, 2)

		C = patch.CollectionMatrix(3)
		self.assertEqual(C.nspecies, 3)

	def test_call(self):
		C = patch.CollectionMatrix(2)

		fail = False
		try:
			C((0,0,0),(0,0,0))
		except:
			fail = True
		self.assertTrue(fail)

		fail = False
		try:
			ans = C((0,0),(0,0))
		except:
			fail = True
		self.assertTrue(not fail)
		self.assertAlmostEqual(ans, 0.0)

	def test_init(self):
		C = patch.CollectionMatrix(2)

		for i in range(0,10):
			for j in range(10,20):
				self.assertAlmostEqual(C((i,j),(i,j+1)), 0.0)
				self.assertAlmostEqual(C((i,j),(i,j-1)), 0.0)
				self.assertAlmostEqual(C((i,j),(i+1,j)), 0.0)
				self.assertAlmostEqual(C((i,j),(i-1,j)), 0.0)

				self.assertAlmostEqual(C((i+1,j),(i,j)), 0.0)
				self.assertAlmostEqual(C((i-1,j),(i,j)), 0.0)
				self.assertAlmostEqual(C((i,j+1),(i,j)), 0.0)
				self.assertAlmostEqual(C((i,j-1),(i,j)), 0.0)

	def test_fast_call(self):
		C = patch.CollectionMatrix(2)

		fail = False
		try:
			ans = C._fast_call((0,0),(0,0))
		except Exception as e:
			print e
			fail = True
		self.assertTrue(not fail)
		self.assertAlmostEqual(ans, 0.0)

		for i in range(0,10):
			for j in range(10,20):
				self.assertAlmostEqual(C._fast_call((i,j),(i,j+1)), 0.0)
				self.assertAlmostEqual(C._fast_call((i,j),(i,j-1)), 0.0)
				self.assertAlmostEqual(C._fast_call((i,j),(i+1,j)), 0.0)
				self.assertAlmostEqual(C._fast_call((i,j),(i-1,j)), 0.0)

				self.assertAlmostEqual(C._fast_call((i+1,j),(i,j)), 0.0)
				self.assertAlmostEqual(C._fast_call((i-1,j),(i,j)), 0.0)
				self.assertAlmostEqual(C._fast_call((i,j+1),(i,j)), 0.0)
				self.assertAlmostEqual(C._fast_call((i,j-1),(i,j)), 0.0)

	def test_update(self):
		C = patch.CollectionMatrix(2)
		C.update((0,0), (0,1), 0.5)
		self.assertAlmostEqual(C((0,0), (0,1)), 0.5)
		self.assertAlmostEqual(C((0,0), (0,0)), 0.5)

		C.update((0,0), (0,1), 0.5)
		self.assertAlmostEqual(C((0,0), (0,1)), 1.0)
		self.assertAlmostEqual(C((0,0), (0,0)), 1.0)

		self.assertAlmostEqual(C((0,0), (0,2)), 0.0)

	def test_set(self):
		C = patch.CollectionMatrix(2)
		C.set((0,0), (0,1), 123)
		self.assertAlmostEqual(C((0,0), (0,1)), 123)
		self.assertAlmostEqual(C((0,0), (0,0)), 0)
		self.assertAlmostEqual(C((0,0), (1,0)), 0)

		fail = False
		try:
			C.set((0,0), (0,1), 123, overwrite=False)
		except:
			fail = True
		self.assertTrue(fail)

		fail = False
		try:
			C.set((0,0), (0,1), 123, overwrite=True)
		except:
			fail = True
		self.assertTrue(not fail)

	def test_transitions(self):
		C = patch.CollectionMatrix(2)
		C.set((0,0), (0,1), 1)
		self.assertTrue(C.transitions((0,0)) == {(0,1):1})

		C.set((0,0), (1,0), 2)
		self.assertTrue(C.transitions((0,0)) == {(0,1):1, (1,0):2})

		self.assertTrue(C.transitions((1,1)) == {})

	def test_lnP(self):
		C = patch.CollectionMatrix(2)
		C.set((0,0), (0,1), 1)
		C.set((0,0), (1,0), 2)
		self.assertAlmostEqual(C.lnP((0,0), (0,1)), np.log(1.0/(1.0+2.0)))

		# cached
		C.set((0,0), (10,10), 3) # fictitious, but a good test
		self.assertAlmostEqual(C.lnP((0,0), (0,1)), np.log(1.0/(1.0+2.0)))

		# recalculate
		self.assertAlmostEqual(C.lnP((0,0), (0,1), cache=False), np.log(1.0/(1.0+2.0+3.0)))

		# cache was updated
		self.assertAlmostEqual(C.lnP((0,0), (0,1)), np.log(1.0/(1.0+2.0+3.0)))

		# wrong number of components
		fail = False
		try:
			C.lnP((0,0,0), (0,0,1))
		except:
			fail = True
		self.assertTrue(fail)

		# no transitions measured
		fail = False
		try:
			C.lnP((2,2),(2,3))
		except:
			fail = True
		self.assertTrue(fail)

	def test_fast_lnP(self):
		C = patch.CollectionMatrix(2)
		C.set((0,0), (0,1), 1)
		C.set((0,0), (1,0), 2)
		self.assertAlmostEqual(C._fast_lnP((0,0), (0,1)), np.log(1.0/(1.0+2.0)))

		# cached
		C.set((0,0), (10,10), 3) # fictitious, but a good test
		self.assertAlmostEqual(C._fast_lnP((0,0), (0,1)), np.log(1.0/(1.0+2.0)))

		# recalculate
		self.assertAlmostEqual(C._fast_lnP((0,0), (0,1), cache=False), np.log(1.0/(1.0+2.0+3.0)))

		# cache was updated
		self.assertAlmostEqual(C._fast_lnP((0,0), (0,1)), np.log(1.0/(1.0+2.0+3.0)))

		# wrong number of components
		fail = False
		try:
			C._fast_lnP((0,0,0), (0,0,1))
		except:
			fail = True
		self.assertTrue(fail)

		# no transitions measured
		fail = False
		try:
			C._fast_lnP((2,2),(2,3))
		except:
			fail = True
		self.assertTrue(fail)

if __name__ == '__main__':
	unittest.main()
