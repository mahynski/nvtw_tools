import unittest
import sys
import os
import numpy as np
sys.path.append('../')
import patch

class TestInitialGuess(unittest.TestCase):
    def setUp(self):
        self.fname = "data/dummy_matrix.txt"

    def test_simple_init(self):
        fail = False
        try:
            res = patch.create_binary_collection_matrix(self.fname)
        except Exception as e:
            fail = True
        self.assertTrue(not fail)

        self.assertTrue(isinstance(res['collection_matrix'], patch.CollectionMatrix))
        self.assertTrue(isinstance(res['bounds'], list))
        self.assertTrue(isinstance(res['limiting_Nf'], dict))

        C, bounds, Nf = res['collection_matrix'], res['bounds'], res['limiting_Nf']

        self.assertTrue(C.nspecies, 2)
        self.assertTrue(bounds == [(0, 2), (0, 2)])
        nf = {}
        for i in range(3):
            nf[(i,)] = 2-i
        self.assertTrue(Nf == nf)

    def test_simple_values_c(self):
        res = patch.create_binary_collection_matrix(self.fname)
        C = res['collection_matrix']

        vals = [1.0, 1.0, 4.0, 1.0, 2.0, 9.0]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            self.assertAlmostEqual(C((n1,n2), (n1+1,n2)), vals[i])

        vals = [2.0+2.0, 1.0+1.0, 5.0+5.0, 3.0+1.0, 6.0+6.0, 2.0+2.0]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            self.assertAlmostEqual(C((n1,n2), (n1,n2)), vals[i])

        vals = [0.0, 0.0, 6.0, 8.0, 0.0, 7.0]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            self.assertAlmostEqual(C((n1,n2), (n1-1,n2)), vals[i])

        vals = [1.0, 1.0, 6.0, 6.0, 2.0, 1.0]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            self.assertAlmostEqual(C((n1,n2), (n1,n2+1)), vals[i])

        vals = [0.0, 1.0, 0.0, 3.0, 1.0, 0.0]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            self.assertAlmostEqual(C((n1,n2), (n1,n2-1)), vals[i])

    def test_simple_values_p(self):
        res = patch.create_binary_collection_matrix(self.fname)
        C = res['collection_matrix']

        vals = [0.1666666667, 0.2, 0.1538461538, 0.0454545455, 0.1176470588, 0.4285714286]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            if (vals[i] > 0.0):
                self.assertAlmostEqual(C.lnP((n1,n2), (n1+1,n2)), np.log(vals[i]))
            else:
                self.assertEqual(C.lnP((n1,n2), (n1+1,n2)), np.finfo(np.float64).min)

        vals = [0.6666666667, 0.4, 0.3846153846, 0.1818181818, 0.7058823529, 0.1904761905]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            if (vals[i] > 0.0):
                self.assertAlmostEqual(C.lnP((n1,n2), (n1,n2)), np.log(vals[i]))
            else:
                self.assertEqual(C.lnP((n1,n2), (n1,n2)), np.finfo(np.float64).min)

        vals = [0, 0, 0.2307692308, 0.3636363636, 0, 0.3333333333]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            if (vals[i] > 0.0):
                self.assertAlmostEqual(C.lnP((n1,n2), (n1-1,n2)), np.log(vals[i]))
            else:
                self.assertEqual(C.lnP((n1,n2), (n1-1,n2)), np.finfo(np.float64).min)

        vals = [0.1666666667, 0.2, 0.2307692308, 0.2727272727, 0.1176470588, 0.0476190476]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            if (vals[i] > 0.0):
                self.assertAlmostEqual(C.lnP((n1,n2), (n1,n2+1)), np.log(vals[i]))
            else:
                self.assertEqual(C.lnP((n1,n2), (n1,n2+1)), np.finfo(np.float64).min)

        vals = [0, 0.2, 0, 0.1363636364, 0.0588235294, 0]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            if (vals[i] > 0.0):
                self.assertAlmostEqual(C.lnP((n1,n2), (n1,n2-1)), np.log(vals[i]))
            else:
                self.assertEqual(C.lnP((n1,n2), (n1,n2-1)), np.finfo(np.float64).min)


    def test_simple_values_p_fast(self):
        res = patch.create_binary_collection_matrix(self.fname)
        C = res['collection_matrix']

        vals = [0.1666666667, 0.2, 0.1538461538, 0.0454545455, 0.1176470588, 0.4285714286]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            if (vals[i] > 0.0):
                self.assertAlmostEqual(C._fast_lnP((n1,n2), (n1+1,n2)), np.log(vals[i]))
            else:
                self.assertEqual(C._fast_lnP((n1,n2), (n1+1,n2)), np.finfo(np.float64).min)

        vals = [0.6666666667, 0.4, 0.3846153846, 0.1818181818, 0.7058823529, 0.1904761905]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            if (vals[i] > 0.0):
                self.assertAlmostEqual(C._fast_lnP((n1,n2), (n1,n2)), np.log(vals[i]))
            else:
                self.assertEqual(C._fast_lnP((n1,n2), (n1,n2)), np.finfo(np.float64).min)

        vals = [0, 0, 0.2307692308, 0.3636363636, 0, 0.3333333333]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            if (vals[i] > 0.0):
                self.assertAlmostEqual(C._fast_lnP((n1,n2), (n1-1,n2)), np.log(vals[i]))
            else:
                self.assertEqual(C._fast_lnP((n1,n2), (n1-1,n2)), np.finfo(np.float64).min)

        vals = [0.1666666667, 0.2, 0.2307692308, 0.2727272727, 0.1176470588, 0.0476190476]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            if (vals[i] > 0.0):
                self.assertAlmostEqual(C._fast_lnP((n1,n2), (n1,n2+1)), np.log(vals[i]))
            else:
                self.assertEqual(C._fast_lnP((n1,n2), (n1,n2+1)), np.finfo(np.float64).min)

        vals = [0, 0.2, 0, 0.1363636364, 0.0588235294, 0]
        for i, (n1,n2) in enumerate([(0,0), (0,1), (1,0), (1,1), (0,2), (2,0)]):
            if (vals[i] > 0.0):
                self.assertAlmostEqual(C._fast_lnP((n1,n2), (n1,n2-1)), np.log(vals[i]))
            else:
                self.assertEqual(C._fast_lnP((n1,n2), (n1,n2-1)), np.finfo(np.float64).min)

    def test_macrostate(self):
        lnPI_dict = patch.binary_initial_guess(**patch.create_binary_collection_matrix(self.fname))

        lnPI_manual = {
        0:{0:0, 1:-0.1823215568, 2:1.0414538748},
        1:{0:-0.3254224004, 1:0.2006706955},
        2:{0:-1.0986122887}
        }

        for n1 in lnPI_manual:
            for n2 in lnPI_manual[n1]:
                self.assertAlmostEqual(lnPI_manual[n1][n2], lnPI_dict[n1][n2])

        self.assertTrue(lnPI_manual.keys() == lnPI_dict.keys())
        for n1 in lnPI_manual:
            self.assertTrue(lnPI_manual[n1].keys() == lnPI_dict[n1].keys())

    def test_sanity_check(self):
        res = patch.create_binary_collection_matrix(self.fname)

        missing = patch.binary_sanity_check(2, res['collection_matrix'])
        self.assertTrue(missing == [])

        missing = patch.binary_sanity_check(3, res['collection_matrix'])
        self.assertTrue(missing == [(0, 3), (1, 2), (2, 1), (3, 0)])

class TestCreateBinaryCollectionMatrixSingleFile(unittest.TestCase):
    def setUp(self):
        pass

    def test_create(self):
        fname = "data/all_data_MOF950_C3H8-C3H6_323.txt"
        fail = False
        try:
            res = patch.create_binary_collection_matrix(fname)
        except Exception as e:
            fail = True
        self.assertTrue(not fail)

        self.assertTrue(isinstance(res['collection_matrix'], patch.CollectionMatrix))
        self.assertTrue(isinstance(res['bounds'], list))
        self.assertTrue(isinstance(res['limiting_Nf'], dict))

        C, bounds, Nf = res['collection_matrix'], res['bounds'], res['limiting_Nf']

        self.assertTrue(C.nspecies, 2)
        self.assertTrue(bounds == [(0, 59), (0, 59)])
        nf = {}
        for i in range(60):
            nf[(i,)] = 59-i
        self.assertTrue(Nf == nf)

        # look at one random point (18,2)
        a, b, c = 3.596523509815e+04, 4.417791032395e+04, 8.545778989809e-01
        self.assertAlmostEqual(C((18,2),(18+1,2)), a)
        self.assertAlmostEqual(C((18,2),(18-1,2)), c)

        d, e, f = 3.651550750505e+04, 4.362836969053e+04, 1.228044229925e-01
        self.assertAlmostEqual(C((18,2),(18,2+1)), d)
        self.assertAlmostEqual(C((18,2),(18,2-1)), f)

        self.assertAlmostEqual(C((18,2),(18,2)), b+e)

    def test_duplicate(self):
        fname = "data/bad_all_data_MOF950_C3H8-C3H6_323.txt" # duplicated a line
        fail = False
        try:
            res = patch.create_binary_collection_matrix(fname)
        except Exception as e:
            fail = True
        self.assertTrue(fail)

    def test_bad_nspecies(self):
        fname = "data/bad2_all_data_MOF950_C3H8-C3H6_323.txt" # one line as N = 3
        fail = False
        try:
            res = patch.create_binary_collection_matrix(fname)
        except Exception as e:
            fail = True
        self.assertTrue(fail)

class TestCreateBinaryCollectionMatrixDictionary(unittest.TestCase):
    def setUp(self):
        pass

    def test_create(self):
        d = {(1,5): "data/output_MOF950_raspa_1.1.1_300.000000_0_N1-1_N2-5.NVTpW_acc_ener"}

        fail = False
        try:
            res = patch.create_binary_collection_matrix(d)
        except Exception as e:
            fail = True
        self.assertTrue(not fail)

        self.assertTrue(isinstance(res['collection_matrix'], patch.CollectionMatrix))
        self.assertTrue(isinstance(res['bounds'], list))
        self.assertTrue(isinstance(res['limiting_Nf'], dict))

        C, bounds, Nf = res['collection_matrix'], res['bounds'], res['limiting_Nf']

        self.assertTrue(C.nspecies, 2)
        self.assertTrue(bounds == [(1, 1), (5, 5)])
        self.assertTrue(Nf == {(1,):5})

        a, b, c = 3.969503883368e+04, 4.002640028503e+04, 5.608812916844e-01
        self.assertAlmostEqual(C((1,5),(1+1,5)), a)
        self.assertAlmostEqual(C((1,5),(1-1,5)), c)

        d, e, f = 3.981325388253e+04, 3.990702971950e+04, 1.716397963036e+00
        self.assertAlmostEqual(C((1,5),(1,5+1)), d)
        self.assertAlmostEqual(C((1,5),(1,5-1)), f)

        self.assertAlmostEqual(C((1,5),(1,5)), b+e)

    def test_mismatch(self):
        d = {(1,6): "data/output_MOF950_raspa_1.1.1_300.000000_0_N1-1_N2-5.NVTpW_acc_ener"}

        fail = False
        try:
            res = patch.create_binary_collection_matrix(d)
        except Exception as e:
            fail = True
        self.assertTrue(fail)

    def test_bad_nspecies(self):
        d = {(1,1,1): "data/output_MOF950_raspa_1.1.1_300.000000_0_N1-1_N2-5.NVTpW_acc_ener"}

        fail = False
        try:
            res = patch.create_binary_collection_matrix(d)
        except Exception as e:
            fail = True
        self.assertTrue(fail)

if __name__ == '__main__':
    unittest.main()
