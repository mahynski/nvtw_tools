import unittest
import sys
import os
import numpy as np
sys.path.append('../')
import patch

class TestOther(unittest.TestCase):
	def setUp(self):
		pass

	def test_spec_exp(self):
		a = 1.234
		b = 2.345
		c = patch._spec_exp(a,b)

		self.assertAlmostEqual(np.log(np.exp(a)+np.exp(b)), c)

		a = -1.234
		b = 2.345
		c = patch._spec_exp(a,b)

		self.assertAlmostEqual(np.log(np.exp(a)+np.exp(b)), c)

		a = 1.234
		b = -2.345
		c = patch._spec_exp(a,b)

		self.assertAlmostEqual(np.log(np.exp(a)+np.exp(b)), c)

		a = -1.234
		b = -2.345
		c = patch._spec_exp(a,b)

		self.assertAlmostEqual(np.log(np.exp(a)+np.exp(b)), c)

if __name__ == '__main__':
	unittest.main()
