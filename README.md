# nvtw_tools : Tools for Performing Multicomponent NVT+W Calculations

This is nvtw_tools, a toolkit for performing calculations using the NVT+W multicomponent
extrapolation approach.

## Getting started

* To install the required packages automatically with pip, type:

~~~
  $ pip install -r requirements.txt
~~~

* Building the nvtw_tools package can be accomplished by executing the shell script
  `install` in the package root directory.

~~~
  $ ./install
~~~

## Unittests

~~~
  $ cd unittest; python run_tests.py
~~~

## Example

~~~
  >>> ans = create_binary_collection_matrix("output.txt")
  >>> assert(binary_sanity_check(Ntot, ans['collection_matrix']) == []), 'missing points!'
  >>> res, final_lnPI = compute_binary_macrostate_distribution(
          options={'disp':True, 'maxiter':3000, 'return_all':True},
          initial_guess=binary_initial_guess(**ans),
          **ans)
~~~
