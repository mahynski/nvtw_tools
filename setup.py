#BUILD:  python setup.py build_ext --inplace

from numpy.distutils.core import setup
from numpy.distutils.core import Extension
from Cython.Distutils import build_ext
import numpy as np
import Cython.Compiler.Options
Cython.Compiler.Options.annotate = True

patch = Extension("patch", ["patch.pyx"],
	include_dirs=[np.get_include()],
	libraries=["m"])

ext_modules = [patch]

for e in ext_modules:
    e.cython_directives = {"embedsignature": True}

setup(cmdclass={'build_ext': build_ext},
	ext_modules=ext_modules)
